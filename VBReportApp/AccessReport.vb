﻿Imports System.Data.OleDb
Imports System.Text

Public Class AccessReport
    Dim connectionString As String = "Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\DeepanM\Employees.mdb"

    Private Sub btnReport_Click(sender As Object, e As EventArgs) Handles btnReport.Click
        Dim conn As New OleDbConnection(connectionString)
        Dim da As New OleDbDataAdapter

        Try
            conn.Open()
            Dim sbSql As New StringBuilder
            sbSql.Append("select * from Employees")

            If Not String.IsNullOrEmpty(txtName.Text.Trim()) Then
                sbSql.Append(" where EmpName = '" + txtName.Text + "'")
            End If

            da = New OleDbDataAdapter(sbSql.ToString(), conn)
            Dim ds As New DataSet
            da.Fill(ds)
            dgvReport.DataSource = ds.Tables(0)

        Catch ex As Exception
            MsgBox(ex.Message)
        Finally
            conn.Close()
            conn.Dispose()
            da.Dispose()
        End Try
    End Sub
End Class
