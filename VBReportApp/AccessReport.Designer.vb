﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AccessReport
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Active = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.btnReport = New System.Windows.Forms.Button()
        Me.label5 = New System.Windows.Forms.Label()
        Me.Name = New System.Windows.Forms.Label()
        Me.label3 = New System.Windows.Forms.Label()
        Me.txtDepartment = New System.Windows.Forms.ComboBox()
        Me.cboCode = New System.Windows.Forms.ComboBox()
        Me.toDate = New System.Windows.Forms.DateTimePicker()
        Me.Designation = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmpName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.EmpCode = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.label2 = New System.Windows.Forms.Label()
        Me.panel2 = New System.Windows.Forms.Panel()
        Me.dgvReport = New System.Windows.Forms.DataGridView()
        Me.Department = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fromDate = New System.Windows.Forms.DateTimePicker()
        Me.label1 = New System.Windows.Forms.Label()
        Me.panel1 = New System.Windows.Forms.Panel()
        Me.panel2.SuspendLayout()
        CType(Me.dgvReport, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Active
        '
        Me.Active.DataPropertyName = "Active"
        Me.Active.HeaderText = "Active"
        Me.Active.Name = "Active"
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(86, 50)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(152, 21)
        Me.txtName.TabIndex = 11
        '
        'btnReport
        '
        Me.btnReport.Location = New System.Drawing.Point(86, 190)
        Me.btnReport.Name = "btnReport"
        Me.btnReport.Size = New System.Drawing.Size(152, 23)
        Me.btnReport.TabIndex = 10
        Me.btnReport.Text = "Show Report"
        Me.btnReport.UseVisualStyleBackColor = True
        '
        'label5
        '
        Me.label5.AutoSize = True
        Me.label5.Location = New System.Drawing.Point(15, 90)
        Me.label5.Name = "label5"
        Me.label5.Size = New System.Drawing.Size(65, 13)
        Me.label5.TabIndex = 9
        Me.label5.Text = "Department"
        '
        'Name
        '
        Me.Name.AutoSize = True
        Me.Name.Location = New System.Drawing.Point(15, 56)
        Me.Name.Name = "Name"
        Me.Name.Size = New System.Drawing.Size(35, 13)
        Me.Name.TabIndex = 8
        Me.Name.Text = "Name"
        '
        'label3
        '
        Me.label3.AutoSize = True
        Me.label3.Location = New System.Drawing.Point(15, 20)
        Me.label3.Name = "label3"
        Me.label3.Size = New System.Drawing.Size(31, 13)
        Me.label3.TabIndex = 7
        Me.label3.Text = "Code"
        '
        'txtDepartment
        '
        Me.txtDepartment.FormattingEnabled = True
        Me.txtDepartment.Location = New System.Drawing.Point(86, 85)
        Me.txtDepartment.Name = "txtDepartment"
        Me.txtDepartment.Size = New System.Drawing.Size(152, 21)
        Me.txtDepartment.TabIndex = 6
        '
        'cboCode
        '
        Me.cboCode.FormattingEnabled = True
        Me.cboCode.Location = New System.Drawing.Point(86, 15)
        Me.cboCode.Name = "cboCode"
        Me.cboCode.Size = New System.Drawing.Size(152, 21)
        Me.cboCode.TabIndex = 4
        '
        'toDate
        '
        Me.toDate.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.toDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.toDate.Location = New System.Drawing.Point(86, 155)
        Me.toDate.Name = "toDate"
        Me.toDate.Size = New System.Drawing.Size(152, 21)
        Me.toDate.TabIndex = 3
        '
        'Designation
        '
        Me.Designation.DataPropertyName = "Designation"
        Me.Designation.HeaderText = "Designation"
        Me.Designation.Name = "Designation"
        '
        'EmpName
        '
        Me.EmpName.DataPropertyName = "EmpName"
        Me.EmpName.HeaderText = "Name"
        Me.EmpName.Name = "EmpName"
        '
        'EmpCode
        '
        Me.EmpCode.DataPropertyName = "Code"
        Me.EmpCode.HeaderText = "Code"
        Me.EmpCode.Name = "EmpCode"
        '
        'ID
        '
        Me.ID.DataPropertyName = "ID"
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        '
        'label2
        '
        Me.label2.AutoSize = True
        Me.label2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label2.Location = New System.Drawing.Point(15, 158)
        Me.label2.Name = "label2"
        Me.label2.Size = New System.Drawing.Size(42, 13)
        Me.label2.TabIndex = 2
        Me.label2.Text = "To Date"
        '
        'panel2
        '
        Me.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel2.Controls.Add(Me.dgvReport)
        Me.panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panel2.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panel2.Location = New System.Drawing.Point(255, 0)
        Me.panel2.Name = "panel2"
        Me.panel2.Size = New System.Drawing.Size(545, 450)
        Me.panel2.TabIndex = 3
        '
        'dgvReport
        '
        Me.dgvReport.AllowUserToAddRows = False
        Me.dgvReport.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvReport.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvReport.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.EmpCode, Me.EmpName, Me.Designation, Me.Department, Me.Active})
        Me.dgvReport.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvReport.Location = New System.Drawing.Point(0, 0)
        Me.dgvReport.Name = "dgvReport"
        Me.dgvReport.RowHeadersVisible = False
        Me.dgvReport.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvReport.Size = New System.Drawing.Size(543, 448)
        Me.dgvReport.TabIndex = 0
        '
        'Department
        '
        Me.Department.DataPropertyName = "Department"
        Me.Department.HeaderText = "Department"
        Me.Department.Name = "Department"
        '
        'fromDate
        '
        Me.fromDate.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fromDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.fromDate.Location = New System.Drawing.Point(86, 120)
        Me.fromDate.Name = "fromDate"
        Me.fromDate.Size = New System.Drawing.Size(152, 21)
        Me.fromDate.TabIndex = 1
        '
        'label1
        '
        Me.label1.AutoSize = True
        Me.label1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.label1.Location = New System.Drawing.Point(15, 124)
        Me.label1.Name = "label1"
        Me.label1.Size = New System.Drawing.Size(56, 13)
        Me.label1.TabIndex = 0
        Me.label1.Text = "From Date"
        '
        'panel1
        '
        Me.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel1.Controls.Add(Me.txtName)
        Me.panel1.Controls.Add(Me.btnReport)
        Me.panel1.Controls.Add(Me.label5)
        Me.panel1.Controls.Add(Me.Name)
        Me.panel1.Controls.Add(Me.label3)
        Me.panel1.Controls.Add(Me.txtDepartment)
        Me.panel1.Controls.Add(Me.cboCode)
        Me.panel1.Controls.Add(Me.toDate)
        Me.panel1.Controls.Add(Me.label2)
        Me.panel1.Controls.Add(Me.fromDate)
        Me.panel1.Controls.Add(Me.label1)
        Me.panel1.Dock = System.Windows.Forms.DockStyle.Left
        Me.panel1.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.panel1.Location = New System.Drawing.Point(0, 0)
        Me.panel1.Name = "panel1"
        Me.panel1.Size = New System.Drawing.Size(255, 450)
        Me.panel1.TabIndex = 2
        '
        'AccessReport
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.panel2)
        Me.Controls.Add(Me.panel1)
        Me.Text = "Access Report"
        Me.panel2.ResumeLayout(False)
        CType(Me.dgvReport, System.ComponentModel.ISupportInitialize).EndInit()
        Me.panel1.ResumeLayout(False)
        Me.panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Private WithEvents Active As DataGridViewCheckBoxColumn
    Private WithEvents txtName As TextBox
    Private WithEvents btnReport As Button
    Private WithEvents label5 As Label
    Private WithEvents Name As Label
    Private WithEvents label3 As Label
    Private WithEvents txtDepartment As ComboBox
    Private WithEvents cboCode As ComboBox
    Private WithEvents toDate As DateTimePicker
    Private WithEvents Designation As DataGridViewTextBoxColumn
    Private WithEvents EmpName As DataGridViewTextBoxColumn
    Private WithEvents EmpCode As DataGridViewTextBoxColumn
    Private WithEvents ID As DataGridViewTextBoxColumn
    Private WithEvents label2 As Label
    Private WithEvents panel2 As Panel
    Private WithEvents dgvReport As DataGridView
    Private WithEvents Department As DataGridViewTextBoxColumn
    Private WithEvents fromDate As DateTimePicker
    Private WithEvents label1 As Label
    Private WithEvents panel1 As Panel
End Class
