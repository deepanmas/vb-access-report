﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Data.OleDb;

namespace VBAccessApp
{
    public partial class AccessReport : Form
    {
        //Change the path of connection string to tha Access file actual location
        private const string connectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\DeepanM\Employees.mdb";
        public AccessReport()
        {
            InitializeComponent();
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            using (OleDbConnection conn = new OleDbConnection(connectionString))
            {
                conn.Open();
                StringBuilder sbSQL = new StringBuilder();
                sbSQL.Append("select * from Employees");
                if (!string.IsNullOrEmpty(txtName.Text.Trim()))
                {
                    sbSQL.Append(" where EmpName = '" + txtName.Text + "'");
                }
                OleDbDataAdapter da = new OleDbDataAdapter(sbSQL.ToString(), conn);
                DataSet ds = new DataSet();
                da.Fill(ds);
                dgvReport.DataSource = ds.Tables[0];
            }

        }
    }
}
